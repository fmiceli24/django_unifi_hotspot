from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

class HomeView(LoginRequiredMixin, TemplateView):
    template_name='visitor/landing-index.html'
    redirect_field_name = 'url'

    def get_context_data(self, **kwargs):
        """Update view context."""
        context = super(HomeView, self).get_context_data(**kwargs)
        try:
            _redirect_url = self.request.GET.get('next', '')
            context.update({
                'redirect_url': _redirect_url
            })

        except Exception as exp_debug:
            print("EXCEPTION: {}".format(str(exp_debug)))
            pass

        return context



