from django.contrib import admin
from .models import UnifiSite, UnifiUser, Device

class UnifiSiteAdmin(admin.ModelAdmin):
    pass
admin.site.register(UnifiSite, UnifiSiteAdmin)

class UnifiUserAdmin(admin.ModelAdmin):
    list_display = ('user', 'phone_number', 'last_backend_login', 'site', 'user_devices')

    def user_first_name(self, obj):
        return obj.user.username

    def user_devices(self,obj):
        return "; ".join([d.guest_mac for d in obj.devices.all()])

admin.site.register(UnifiUser, UnifiUserAdmin)
admin.site.register(Device)
