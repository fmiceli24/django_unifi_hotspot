from __future__ import unicode_literals
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth.models import User


class Device(models.Model):
    guest_mac = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.guest_mac

class UnifiUser(models.Model):
    """Base module registered unifi user."""

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = PhoneNumberField()
    site = models.SlugField(max_length=25)
    last_backend_login = models.CharField(max_length=30, null=True)
    devices = models.ManyToManyField(Device)

    def __unicode__(self):
        return self.user.username

    def __str__(self):
        return self.user.username

    class Meta:
        #app_label = 'django_unifi_hotspot'
        permissions = (
            ("can_navigate", "Can Navigate"),
        )


class UnifiSite(models.Model):
    name = models.CharField(max_length=96)
    unifi_id = models.SlugField(max_length=25)

    def __str__(self):
        return "{}({})".format(self.name, self.unifi_id)
