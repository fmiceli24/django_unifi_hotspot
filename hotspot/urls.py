from django.conf.urls import url, include
from . import views

app_name = 'hotspot'

urlpatterns = [
    url(r'^s/(?P<unifi_site>[\w-]+)/$', views.GuestRequestView.as_view(), name='guest_request'),
]
