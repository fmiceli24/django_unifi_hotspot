import time
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.conf import settings
from django.views.generic import TemplateView, UpdateView, CreateView, ListView
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.contrib.auth.models import User
from .models import UnifiSite, Device, UnifiUser
from .unifi_client import UnifiClient

class GuestRequestView(LoginRequiredMixin, TemplateView):
    template_name = 'visitor/landing-index.html'
    #redirect_field_name = 'next'

    def get_user_profile(self):
        user_profile = None;
        user = User.objects.get(username=self.request.user)
        print("user {}".format(self.request.user))
        try:
            user_profile = UnifiUser.objects.get_or_create(user=user)[0]
        except:
            user_profile = None;
        return user_profile;

    def get_context_data(self, **kwargs):
        """Update view context."""
        context = super(GuestRequestView, self).get_context_data(**kwargs)
        print("args: {}".format(kwargs))
        try:
            _mac = self.request.GET.get('id', '')
            _ap = self.request.GET.get('ap', '')
            _url = self.request.GET.get('url', '')
            # _t = self.request.GET.get('t', '')
            _t = settings.UNIFI_TIMEOUT_MINUTES
            _last_login = time.strftime("%c")
            _site = kwargs['unifi_site']

            context.update({
                'guest_mac': _mac,
                'ap_mac': _ap,
                'minutes': _t,
                'url': _url,
                'last_login': _last_login,
                'site': _site
            })

            # # Saving info on userprofile Model
            userprofile = self.get_user_profile()
            print("userprofile {}".format(userprofile))

            if userprofile:
                 # userprofile.devices.add(Device.objects.get_or_create(guest_mac=_mac)[0])
                 userprofile.site = _site
                 userprofile.last_backend_login = _last_login
                 userprofile.save()
                 device = Device.objects.get_or_create(guest_mac=_mac)[0]
                 device.save()
                 userprofile.devices.add(device)
            #
            # Ask authorization to unifi server
            unifi_client = UnifiClient(site_id=_site)
            _status_code = unifi_client.send_authorization(_mac, _ap, _t)
            context.update({'status_code': _status_code})
            print("context-> {}".format(context))

        except Exception as exp_debug:
            print("EXCEPTION: {}".format(str(exp_debug)))
            pass

        return context

    def post(self, request, *args, **kwargs):
        """Deny post requests."""
        return HttpResponseForbidden();

    def get(self, request, *args, **kwargs):
        """Response with rendered html template."""

        context = self.get_context_data(**kwargs)

        if 'status_code' in context and context['status_code'] == 200:
             if 'url' in context:
                 if context['url']: #if i try to go on an url without wifi login
                     return HttpResponseRedirect('/?next={}'.format(context['url']))

        return self.render_to_response(context)

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(GuestRequestView, self).dispatch(request, *args, **kwargs)
