from django import forms

from django.core.exceptions import ValidationError
from django.forms.fields import CharField
from django.utils.translation import ugettext_lazy as _

from phonenumber_field.phonenumber import to_python
from phonenumber_field.validators import validate_international_phonenumber

#from phonenumber_field.formfields import PhoneNumberField
from .models import UnifiUser

class PhoneNumberField(CharField):
    default_error_messages = {
        #'invalid': _('Enter a valid phone number.'),
        'invalid': _('Ingrese un número de teléfono válido.'),

    }
    default_validators = [validate_international_phonenumber]

    def __init__(self, *args, **kwargs):
        super(PhoneNumberField, self).__init__(*args, **kwargs)
        self.widget.input_type = 'tel'

    def to_python(self, value):
        phone_number = to_python(value)
        if phone_number and not phone_number.is_valid():
            raise ValidationError(self.error_messages['invalid'])
        return phone_number

class SignupForm(forms.Form):

    class Meta:
        model = UnifiUser
        fields = ['first_name', 'last_name', 'phone_number']

    first_name = forms.CharField(max_length=30, label='Nombre')
    last_name = forms.CharField(max_length=30, label='Apellido')
    phone_number = PhoneNumberField(label="Teléfono")

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        #user.save()
        unifiuser = UnifiUser.objects.create(user=user,phone_number= self.cleaned_data['phone_number'])
        unifiuser.save()
        user.unifiuser = unifiuser
        #user.unifiuser.phone_number = self.cleaned_data['phone_number']
        user.save()
