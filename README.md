# Django Unifi Hotspot

Hotspot para la autenticación de usuarios en la controladora Unifi. Basado en el proyecto [Django-unifi-portal](https://github.com/bsab/django-unifi-portal)

# Características

* Creación y login de usuarios.
* [Allauth](https://github.com/pennersr/django-allauth) para login social.
* [Bootstrap3](https://github.com/dyve/django-bootstrap3) para la maquetación del sitio.
* Asocia la mac del dispositivo con el usuario que ingresó a la controladora.

# Instalación

## PostgreSQL

Instalar postgresql y las dependencias para Django

```
sudo apt-get -y install build-essential libpq-dev python-dev
sudo apt-get -y install postgresql postgresql-contrib
```

## Nginx

Se usa nginx para servir la aplicación y todos los archivos estáticos.

```
sudo apt-get -y install nginx
```

## Supervisor

Para "demonizar" la aplicación

```
sudo apt-get -y install supervisor
```

Habilitar e iniciar supervisor

```
sudo systemctl enable supervisor
sudo systemctl start supervisor
```

## Python Virtualenv

Para aislar la instalación de todas las dependencias de la aplicación

```
sudo apt-get -y install python-virtualenv
```

# Configuración

## Configuración de la base de datos PostgreSQL

```
su - postgres
createuser u_numeral
createdb numeral_prod --owner u_numeral
psql -c "ALTER USER u_urban WITH PASSWORD '<password>'"
exit
```

# Configurar usuario para la aplicación

Generar un usuario y agregarlo a sudoers

```
adduser hotspot
gpasswd -a hotspot sudo
```

Cambiarse a ese usuario

```
su - hotspot
```

## Configurar Python Virtualenv

Dentro del home de hotspot se creará el entorno virtual para manejar todas las dependencias de la app.

```
virtualenv numeralenv
```

Activar el entorno virtual:

```
source bin/activate
```

## Clonar el repo:

```
git clone https://ebordon@bitbucket.org/ebordon/django_unifi_hotspot.git
cd django_unifi_hotspot
pip install -r requirements.txt
```

Aplicar los cambios a la base de datos y generar estáticos

```
python manage.py migrate
python manage.py collectstatic
```

## Configurar Gunicorn

```
pip install gunicorn
```

Generar el archivo `~/bin/gunicorn_start` con el siguiente contenido

```
#!/bin/bash

NAME="django_unifi_hotspot"
DIR=/home/hotspot/django_unifi_hotspot
USER=hotspot
GROUP=hotspot
WORKERS=3
BIND=unix:/home/hotspot/run/gunicorn.sock
DJANGO_SETTINGS_MODULE=django_unifi_hotspot.settings
DJANGO_WSGI_MODULE=django_unifi_hotspot.wsgi
LOG_LEVEL=error

cd $DIR
source ../numeralenv/bin/activate

export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DIR:$PYTHONPATH

exec ../numeralenv/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $WORKERS \
  --user=$USER \
  --group=$GROUP \
  --bind=$BIND \
  --log-level=$LOG_LEVEL \
  --log-file=-
```

Dar permisos al archivo

```
chmod u+x bin/gunicorn_start
```

Generar el directorio `run`

```
mkdir ~/run
```

## Configurar Supervisor

```
mkdir ~/logs
touch ~/logs/gunicorn-error.log
```

Generar el archivo de configuración para la app `/etc/supervisor/conf.d/django_unifi_hotspot.conf`

```
[program:django_unifi_hotspot]
command=/home/hotspot/bin/gunicorn_start
user=hotspot
autostart=true
autorestart=true
redirect_stderr=true
stdout_logfile=/home/hotspot/logs/gunicorn-error.log
```

```
sudo supervisorctl reread
sudo supervisorctl update
```

Para chequear el status
```
sudo supervisorctl status django_unifi_hotspot
django_unifi_hotspot                      RUNNING   pid 23381, uptime 0:00:15
```


Para manejar la app como servicio se utiliza el comando `supervisorctl`
```
sudo supervisorctl restart django_unifi_hotspot
```

## Configurar nginx

Crear el archivo `/etc/nginx/sites-available/numeral_hotspot` con el siguiente contenido

```
upstream app_server {
    server unix:/home/hotspot/run/gunicorn.sock fail_timeout=0;
}

server {
    listen 80;

    # add here the ip address of your server
    # or a domain pointing to that ip (like example.com or www.example.com)
    server_name portal.numeral.me;

    keepalive_timeout 5;
    client_max_body_size 4G;

    access_log /home/hotspot/logs/nginx-access.log;
    error_log /home/hotspot/logs/nginx-error.log;

    location /static/ {
        alias /home/hotspot/django_unifi_hotspot/static/;
    }

    # checks for static file, if not found proxy to app
    location / {
        try_files $uri @proxy_to_app;
    }

    location @proxy_to_app {
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_redirect off;
      proxy_pass http://app_server;
    }
}
```

Para finalizar

```
sudo ln -s /etc/nginx/sites-available/numeral_hotspot /etc/nginx/sites-enabled/numeral_hotspot
sudo rm /etc/nginx/sites-enabled/default
sudo service nginx restart
```
